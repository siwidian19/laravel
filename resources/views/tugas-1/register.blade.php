<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laravel - Web Statis</title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h2>Sign Up Form</h2>
  <form id="formdaftar" name="formdaftar" action="/welcome" method="post">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
    <p>
      <label for="firstname">First name:</label>
      <br>
      <br>
      <input type="text" id="firstname" name="firstname">
    </p>
    <p>
      <label for="lastname">Last Name:</label>
      <br>
      <br>
      <input type="text" id="lastname" name="lastname">
    </p>
    <p>
      Gender:
      <br>
      <br>
      <input type="radio" id="male" name="male" value="male">
      <label for="male">Male</label>
      <br>
      <input type="radio" id="female" name="female" value="female">
      <label for="female">Female</label>
      <br>
      <input type="radio" id="other" name="other" value="other">
      <label for="other">Other</label>
    </p>
    <p>
      <label for="nationality">Nationality:</label>
      <br>
      <br>
      <select id="nationality" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="singapura">Singapura</option>
        <option value="malaysia">Malaysia</option>
        <option value="australia">Australia</option>
      </select>
    </p>
    <p>
      Language Spoken:
      <br>
      <br>
      <input type="checkbox" id="bahasa" name="bahasa" value="bahasa">
      <label for="bahasa">Bahasa Indonesia</label>
      <br>
      <input type="checkbox" id="english" name="english" value="english">
      <label for="english">English</label>
      <br>
      <input type="checkbox" id="lain" name="lain" value="lain">
      <label for="lain">Other</label>
    </p>
    <p>
      <label for="bio">Bio:</label>
      <br>
      <br>
      <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
    </p>
    <input type="submit" value="Sign Up">
  </form>
</body>

</html>