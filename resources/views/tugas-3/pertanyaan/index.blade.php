@extends('tugas-2.adminlte.master')

@section('content')
<div class="content pt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">DATA PERTANYAAN</h3>
            </div>
            
            <div class="card-body">
              @if(session('success'))
              <div class="alert alert-success">{{session('success')}}</div>
              @endif
              <div class="float-right">
                <a href="/pertanyaan/create" class="btn btn-info btn-sm mb-2"><i class="far fa-plus-square fa-lg"></i> Pertanyaan Baru</a>
              </div>
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>JUDUL</th>
                    <th>ISI</th>
                    <th class="text-center" style="width: 10px"><i class="fas fa-cog fa-lg"></i></th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($posts as $key => $data)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $data->judul }}</td>
                      <td>{{ $data->isi }}</td>
                      <td class="text-center" style="display:flex;">
                        <a href="/pertanyaan/{{ $data->id }}"><i title="Lihat Data" class="p-1 fas fa-search fa-lg"></i></a>
                        <a href="/pertanyaan/{{ $data->id }}/edit"><i title="Pembaharuan Data" class="p-1 far fa-edit fa-lg"></i></a>
                        <form class="form-horizontal form" action="/pertanyaan/{{ $data->id }}" method="POST">
                          @csrf
                          @method('DELETE')
                          <!-- <a href="/pertanyaan/{{ $data->id }}" data-method="DELETE" data-token="{{csrf_token()}}><i title="Hapus Data" class="p-1 far fa-trash-alt fa-lg"></i></a> -->
                          <button type="submit" class="btn btn-link"><i title="Hapus Data" class="far fa-trash-alt fa-lg"></i></button>
                        </form>
                      </td>
                    </tr>
                  @empty
                    <tr>
                      <td colspan="4" align="center">Data Pertanyaan Tidak Ditemukan!</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>
  </div>
</div>

<style>
  .form{
    margin-top: -5px;
    margin-left: -8px
  }
</style>
@endsection