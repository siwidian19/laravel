@extends('tugas-2.adminlte.master')

@section('content')
<div class="content pt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h3 class="card-title">DETAIL PERTANYAAN</h3>
          </div>         
          <div class="card-body">
            <div class="post">
              <h4><b>{{ $post->judul }}</b></h4>  
              dibuat: <span class="description">{{ $post->tanggal_dibuat }}</span>
              <div class="pt-4"><p>{{ $post->isi }}</p></div>
              diperbaharui: <span class="description">{{ $post->tanggal_diperbaharui }}</span>
            </div> 
          </div>
          <!-- /.card-body -->       
        </div>
        <!-- /.card -->
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
