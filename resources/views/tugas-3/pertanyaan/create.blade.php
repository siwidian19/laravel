@extends('tugas-2.adminlte.master')

@section('content')
<div class="content pt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">KOLOM TAMBAH PERTANYAAN</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="form-horizontal" id="formpertanyaan" name="formpertanyaan" method="POST" action="/pertanyaan">
            @csrf
            <?php
              $today = Carbon\Carbon::now();
            ?>
            <input type="hidden" name="today" value="{{$today}}">
              <div class="card-body">

                <div class="form-group row">
                  <label for="judul" class="col-md-2 col-form-label">Judul</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" id="judul" name= "judul" placeholder="Judul" value="{{old('judul','')}}">
                    @error('judul')
                      <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <label for="isi" class="col-md-2 col-form-label">Isi Pertanyaan</label>
                  <div class="col-md-10">
                    <textarea class="form-control" id="isi" name="isi" rows="6">{{old('isi','')}}</textarea>
                    <!-- <textarea id="summernote" name="isi" class="summernote"></textarea>
                    -->
                    @error('isi')
                      <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                </div>
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-info btn-flat float-right"><i class="fas fa-paper-plane"></i></button>
              </div>
              <!-- /.card-footer -->
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<!-- Summernote
<script src="{{asset('/assets/plugins/summernote/summernote-bs4.min.js')}}"></script>

<script>
  $(function () {  
    $('#summernote').summernote({
      placeholder: 'Pertanyaan',
      tabsize: 1,
      height: 200,
    }).on("summernote.enter", function(we, e) {
$(this).summernote("pasteHTML", "<br><br>");
e.preventDefault();
});
    
    
  })
</script> -->

@endpush
@endsection

