<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('halo', function () {
// 	return "Halo, Selamat datang di tutorial laravel www.malasngoding.com";
// });

// Route::get('blog', function () {
// 	return view('blog');
// });

Route::get('/', 'BerandaController@index');

Route::get('/data-tables', 'TablesController@index');

//Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@showFormRegister');

Route::post('/welcome', 'AuthController@register');

Route::get('/welcome', 'AuthController@welcome');

// Route::get('/', function () {
//     @include('library.library');
//     return view('test.test');
// });

Route::get('/pertanyaan/create','PertanyaanController@create');

Route::post('/pertanyaan','PertanyaanController@store');

Route::get('/pertanyaan','PertanyaanController@index');

Route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');

Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');

Route::put('/pertanyaan/{pertanyaan_id}','PertanyaanController@update');

Route::delete('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy');


