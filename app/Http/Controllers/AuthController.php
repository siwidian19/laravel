<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;

class AuthController extends Controller
{
    public function showFormRegister(){
    	return view('tugas-1.register');
    }

    public function register(Request $request){

        $firstname = $request->input('firstname');
     	$lastname = $request->input('lastname');

        return view('/tugas-1.welcome',['firstname' => $firstname, 'lastname' => $lastname]);
 
    }
}


?>