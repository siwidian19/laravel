<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('tugas-3.pertanyaan.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:posts',
            'isi' => 'required',  
        ]);

        $query = DB::table('posts')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $request["today"],
            "tanggal_diperbaharui" => $request["today"]
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan baru berhasil ditambahkan!');
    }

    public function index(){
        $posts = DB::table('posts')->get();
        return view('tugas-3.pertanyaan.index',compact('posts'));
    }

    public function show($pertanyaan_id){
        $post = DB::table('posts')->where('id',$pertanyaan_id)->first();

        return view('tugas-3.pertanyaan.show', compact('post'));
    }

    public function edit($pertanyaan_id){
        $post = DB::table('posts')->where('id',$pertanyaan_id)->first();

        return view('tugas-3.pertanyaan.edit', compact('post'));
    }

    public function update($pertanyaan_id,Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required' 
        ]);

        $post = DB::table('posts')
                    ->where('id',$pertanyaan_id)
                    ->update([
                        'judul'=> $request['judul'],
                        'isi' => $request['isi'],
                        'tanggal_dibuat' => $request['adddate'],
                        'tanggal_diperbaharui' => $request['updatedate']
                    ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil perbaharui!');
    }

    public function destroy($pertanyaan_id){
        $query = DB::table('posts')->where('id',$pertanyaan_id)->delete();

        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil dihapus!');
    }
}
